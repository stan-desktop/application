![picture](https://i.imgur.com/E2mvAHD.png)

Unofficial Stan application, Open source and multi-platform for all platforms to use.

Enjoy all your Stan content just like on the web version all wrapped up into a desktop application!

&nbsp;&nbsp;&nbsp;&nbsp;

  You can install Youtube from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/stan-desktop/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/stan-desktop/application/-/releases)

 ### Author
  * Corey Bruce